# Device list Example

This folder contains an example of how to use the C++ API to list
all the Inverse3s and VerseGrips currently connected to your computer.

> The executable can take an optional `--mocks` argument to list also virtual
> devices.

```
$ ./bin/00-device-list.exe [--mocks]
```
