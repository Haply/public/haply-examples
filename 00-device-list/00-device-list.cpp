/* -*- mode: c++ -*-
 * Copyright 2023 Haply Robotics Inc. All rights reserved.
 *
 * Lists all the available Inverse3 and VerseGrip devices
 */

#include "haply_inverse.hpp"

int main(int argc, const char *argv[]) {
    // allow --flags argument to list also virtual devices
    auto flags = argc > 1 && std::string{argv[1]} == "--mocks"
                     ? haply_inverse_flag_device_mocks
                     : 0;

    haply::inverse::init();

    try {
        haply::inverse::client client{};

        haply::inverse::check(client.connect(flags), "Unable to connect the client");

        std::vector<haply::inverse::device_id> list = client.device_list().unwrap(
            "Unable to list devices");

        for (auto id : list) {
            haply::inverse::latest latest = client.latest(id).unwrap(
                "Unable to retrieve device (id:"
                + std::to_string(id) + ") state");

            fprintf(stdout, "%s (id:%x, tag:%04x%s)\n",
                    haply::inverse::is_inverse3(latest.device) ? "Inverse3" : "VerseGrip", id,
                    latest.device.tag_id, latest.device.mock ? ", mock" : "");
        }
    }
    catch (const haply::inverse::exception &e){
        fprintf(stderr, "%s\n", e.what());
        return e.cause;
    }

    return 0;
}
