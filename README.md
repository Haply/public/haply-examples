# Example of using the Haply Inverse Service SDK

This repository contains a suite of simple examples for using Haply's Inverse3
and VerseGrip devices using the C++ Haply Inverse Service SDK.

## Prerequisites

In order to use the examples provided in this repository, one has to install the following prerequisites:

1. [cmake](https://cmake.org)
2. [Visual Studio](https://visualstudio.microsoft.com/)
3. [Haply Inverse Runtime](https://develop.haply.co/releases/installer)
    - Version 2.0.0 or later

## Build

Builds are managed through [cmake](https://cmake.org) and can be invoked from
the command line using the following commands:

```shell
$ cmake . -B ./build
$ cmake --build ./build --config Release
$ cmake --install ./build
```

A [`CMakeSettings.json`](./CMakeSettings.json) file is also provided to simplify
builds with [Visual Studio](https://visualstudio.microsoft.com/). You'll need to
manually install the executables once the build is complete using the
menu `Build >
Install HardwareAPI.Examples`.

Note that the build will automatically download the latest version of the C++
API and extract it into the `HardwareAPI` folder. This folder will include both
the static library for the API along with the headers.

## Examples

The output of the build and installation process will be placed in a `bin`
folder located at the root of the repository and will contain an executable for
each of the examples that are described below:

- [`00-device-list`](./00-device-list) Uses the Hardware API service to list all
  the currently connected Inverse3 and VerseGrips

- [`01-print-inverse3`](./01-print-inverse3) Connects to an Inverse3 device and
  outputs its position and velocity values as you move its end-effector.

- [`02-print-verse-grip`](./02-print-verse-grip) Connects to a VerseGrip device and outputs
  its orientation and state values as you move it around.

- [`03-hello-wall`](./03-hello-wall) Simple haptics demo where you can push
  against an infinite plane.

- [`04-combined`](./04-combined) More complex demo that combines both an
  Inverse3 and a VerseGrip.
