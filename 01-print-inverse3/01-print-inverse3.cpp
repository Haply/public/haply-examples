/* -*- mode: c++ -*-
 * Copyright 2023 Haply Robotics Inc. All rights reserved.
 *
 * Simple example of how to use the Inverse3 as a 3D mouse where the position
 * and velocity of the device's end-effector will be sent to stdout as it is
 * moved.
 */

#include "haply_inverse.hpp"

int main(int argc, const char *argv[]) {
    // allow --flags argument to list also virtual devices
    auto flags = argc > 1 && std::string{argv[1]} == "--mocks"
                     ? haply_inverse_flag_device_mocks
                     : 0;
    haply::inverse::init();

    haply::inverse::events events;
    // Adds a callback for each time a new cursor position is received. In this
    // case it prints out the position and velocity.
    events.on_cursor = [](auto &, auto, auto &state) {
        // Printing too fast can cause problems.
        static size_t step = 0;
        if (((++step) % (haply::inverse::inverse3_max_poll_rate / 100)) > 0)
            return;

        std::fprintf(stdout,
                     "\r"
                     "position=[ % 0.3f % 0.3f % 0.3f ] "
                     "velocity=[ % 0.3f % 0.3f % 0.3f ]",
                     state.position[0], state.position[1], state.position[2],
                     state.velocity[0], state.velocity[1], state.velocity[2]);
    };

    haply::inverse::client client{std::move(events)};

    try {
        haply::inverse::check(
            client.connect(flags),
            "Unable to connect the client");

        haply::inverse::device_id device_id =
            client.device_open_first(haply_inverse_device_type_inverse3)
                .unwrap("Unable to connect Inverse3");

        haply::inverse::latest latest = client.latest(device_id)
                                   .unwrap("Unable to retrieve cursor state");

        fprintf(stdout, "Inverse3 (id:%x, tag:%04x%s)\n",
                device_id, latest.device.tag_id,
                latest.device.mock ? ", mock" : "");

        while (true) client.poll().unwrap();
    }
    catch (const haply::inverse::exception &e){
        fprintf(stderr, "%s\n", e.what());
        return e.cause;
    }
}
