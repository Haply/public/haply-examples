# Simple State Printer for Inverse3

This folder contains a simple example of how to connect to an Inverse3 device,
wake it up, receive its general device information, and print the device's end
effector position and velocity as you move it. In essence these are the basics
for using the Inverse3 as a 3D mouse.

> The executable can take an optional `--mocks` argument to list also virtual
> devices.

```
$ ./bin/01-print-inverse3.exe [--mocks]
```
