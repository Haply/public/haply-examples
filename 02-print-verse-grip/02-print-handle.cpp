/* -*- mode: c++ -*-
 * Copyright 2023 Haply Robotics Inc. All rights reserved.
 *
 * Simple example of how to use the Inverse3 as a 3D mouse where the position
 * and velocity of the device's end-effector will be sent to stdout as it is
 * moved.
 */

#include "haply_inverse.hpp"

int main(int argc, const char *argv[]) {
    // allow --flags argument to list also virtual devices
    auto flags = argc > 1 && std::string{argv[1]} == "--mocks"
                     ? haply_inverse_flag_device_mocks
                     : 0;

    haply::inverse::init();

    haply::inverse::events events;
    events.on_verse_grip = [](auto &, auto, auto &state) {
        std::fprintf(stdout,
                     "\r"
                     "attached=%u, buttons=%u, "
                     "quaternion=[ % 0.3f % 0.3f % 0.3f % 0.3f ]",
                     state.attached, state.data.info.button,
                     state.quaternion[0], state.quaternion[1],
                     state.quaternion[2], state.quaternion[3]);
    };

    haply::inverse::client client{std::move(events)};

    try {
        haply::inverse::check(
            client.connect(flags),
            "Unable to connect the client");

        haply::inverse::device_id device_id =
            client.device_open_first(haply_inverse_device_type_verse_grip)
                .unwrap("Unable to connect VerseGrip");

        haply::inverse::latest latest = client.latest(device_id)
                                   .unwrap("Unable to retrieve VerseGrip state");

        fprintf(stdout, "VerseGrip (id:%x, tag:%04x%s)\n",
                device_id, latest.device.tag_id,
                latest.device.mock ? ", mock" : "");

        while (true) client.poll().unwrap();
    }
    catch (const haply::inverse::exception &e){
        fprintf(stderr, "%s\n", e.what());
        return e.cause;
    }
}
