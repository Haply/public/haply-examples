# Simple State Printer for VerseGrip

This folder contains an example of how to connect to a Haply VerseGrip, wake it up,
receive its general device information, and print the device's orientation and
status as you move it. We will focus ourselves on the Quill VerseGrip provided by
Haply. Custom VerseGrip devices are handled in a very similar fashion.

> The executable can take an optional `--mocks` argument to list also virtual
> devices.

```
$ ./bin/02-print-verse-grip.exe [--mocks]
```
