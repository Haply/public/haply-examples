# Combined Inverse3 and VerseGrip Example

This folder contains an example that uses a the orientation of a VerseGrip to move
the end-effector of an Inverse3 using position control. It combines the API for
the Inverse3 and the VerseGrip devices and shows how to use threads and `haply_wait_for` to manage the
difference in polling frequencies between the two.
