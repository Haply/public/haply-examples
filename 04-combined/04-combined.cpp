/* -*- mode: c++ -*-
 * Copyright 2023 Haply Robotics Inc. All rights reserved.
 *
 * Example that moves the end-effector of an Inverse3 using the direction that a
 * VerseGrip is currently pointing to. It shows how to combine both an Inverse3 and
 * a VerseGrip.
 */

#include "haply_inverse.hpp"

// Vector types used throughout the example.
typedef std::array<float, 3> vec3;
typedef std::array<float, 4> vec4;

vec3 to_vec3(float (&source)[3]) { return {source[0], source[1], source[2]}; }

vec4 to_vec4(float (&source)[4]) {
    return {source[0], source[1], source[2], source[3]};
}

// Y-axis runs along the VerseGrip's body and represents the local direction we want
// to move in. Multiplying the rotation matrix by [0;1;0] extracts the Y
// component of the rotation giving us the direction vector:
//
//    R(Q) * [0; 1; 0]
//
// For simplicity and speed, we'll only calculate the middle Y component
// of the rotation matrix.
void quaternion_to_direction(const vec4 &q, vec3 &dir) {
    float s = 0.0;
    for (float i : q)
        s += i * i;
    s = 1 / s;

    // The VerseGrip's quaternion has r,x,y,z order.
    static constexpr size_t r = 0, x = 1, y = 2, z = 3;
    dir[0] = 2 * s * (q[x] * q[y] - q[z] * q[r]);
    dir[1] = 1 - 2 * s * (q[x] * q[x] + q[z] * q[z]);
    dir[2] = 2 * s * (q[y] * q[z] + q[x] * q[r]);
}

int main(int argc, const char *argv[]) {
    haply::inverse::init();

    haply::inverse::client client{};

    try {
        // Connect to the Haply service
        haply::inverse::check(client.connect(), "Unable to connect the client");

        // Run thread
        haply::inverse::thread thread{client};

        // Open the VerseGrip
        auto verse_grip_id = client.device_open_first(haply_inverse_device_type_verse_grip)
                             .unwrap("Unable to connect VerseGrip");
        fprintf(stdout, "VerseGrip found (id:%x)\n", verse_grip_id);

        // Open to Inverse3
        auto inv3_id =
            client.device_open_first(haply_inverse_device_type_inverse3)
                           .unwrap("Unable to connect Inverse3");
        fprintf(stdout, "Inverse3 found (id:%x)\n", inv3_id);

        // Wait user
        fprintf(stdout,
                "\nCalibrate the VerseGrip while positioned parallel to the "
                "Y+ axis of the Inverse3.\n"
                "(to calibrate, press the button on the back of the VerseGrip "
                "until the orange LED blinks)\n\n"
                "When it's done, press ENTER to being...\n");
        while (std::getc(stdin) != '\n')
            ;

        // Read the initial position of the end-effector which we will be
        // adjusting.
        auto latest_inv3 =
            client.latest(inv3_id).unwrap("Unable to retrieve cursor state");
        auto position = to_vec3(latest_inv3.state.cursor.position);

        int nb_err = 0;
        while (thread.error() == haply_inverse_ok) {
            try {
                // Read the latest_inv3 cached orientation quaternion and button
                // state from the VerseGrip thread.
                auto latest_verse_grip = client.latest(verse_grip_id).unwrap(
                    "Unable to retrieve VerseGrip state");
                bool button = latest_verse_grip.state.verse_grip.data.info.button;

                // Transform our quaternion into a direction vector representing
                // where the VerseGrip is pointing.
                vec3 dir;
                quaternion_to_direction(
                    to_vec4(latest_verse_grip.state.verse_grip.quaternion), dir);

                // Compute a new end-effector position which will be updated
                // based on where the VerseGrip is pointing while the VerseGrip button
                // is held down. The multiplication constant used here is
                // somewhat arbitrary and was obtained through trial and error.
                if (button) {
                    for (size_t i = 0; i < 3; ++i)
                        position[i] += dir[i] * 0.00005f;
                }
                auto request = haply::inverse::make_cursor_position(
                    position[0], position[1], position[2]);

                // Update our end-effector position to reflect the desired
                // position.
                haply::inverse::check(client.cursor_set_position(inv3_id, request),
                             "Unable to set cursor position");

                static size_t step = 0;
                if (((++step) % (haply::inverse::inverse3_max_poll_rate / 100)) == 0) {
                    fprintf(stdout,
                            "\r"
                            "move=%u, "
                            "dir=[ % 0.3f % 0.3f % 0.3f ], "
                            "pos=[ % 0.3f % 0.3f % 0.3f ]",
                            button, dir[0], dir[1], dir[2], request.position[0],
                            request.position[1], request.position[2]);
                }

                // Frequency at which we'll update the cursor's position
                haply::inverse::wait_for(1'000'000'000 / haply::inverse::inverse3_max_poll_rate);
            } catch (haply::inverse::exception &e) {
                nb_err++;
                std::fprintf(stderr, "\r%s (retry %d...)", e.what(), nb_err);
                haply::inverse::wait_for(nb_err * 100'000'000'000 /
                                haply::inverse::inverse3_max_poll_rate);
            }
        }
    } catch (haply::inverse::exception &e) {
        fprintf(stderr, "%s\n", e.what());
        return e.cause;
    }

    return 0;
}
