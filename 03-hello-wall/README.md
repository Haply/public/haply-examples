# Simple Haptic Feedback Example

This folder contains an example for a basic haptic feedback loop using the
Inverse3 device. It sets up an arbitrary wall (floor) in the device's workspace
that you can push against.
