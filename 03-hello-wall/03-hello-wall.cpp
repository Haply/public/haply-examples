/* -*- mode: c++ -*-
 * Copyright 2023 Haply Robotics Inc. All rights reserved.
 *
 * Simple haptic feedback loop using an arbitrary wall (floor) in the device's
 * workspace.
 */
#include "haply_inverse.hpp"

// Location of the haptic plane (floor) on the Z axis roughly located in the
// center of the device's workspace.
constexpr float wall = 0.17f;

// Constant used determine the strength of the device's haptic response as the
// user pushes against the wall. The higher the value the harder it will be to
// push through the wall.
constexpr float stiffness = 2500;

int main(int argc, const char *argv[]) {
    haply::inverse::init();

    haply::inverse::events events;

    // Add callback to cursor state event
    events.on_cursor = [](auto &client, auto id, auto &state) {
        // If our end-effector position is positioned below our wall (floor)
        // we'll apply an upward force that is proportional to how deep into the
        // wall the end-effector currently is. The wall is modeled as a stiff
        // spring with a spring constant of 2500N/m. i.e. 1mm penetration
        // results in 2.5N of force.
        float force_z = state.position[2] > wall
                            ? 0.0f
                            : (0.17f - state.position[2]) * stiffness;

        // Send the current force value to the device
        auto cursor_force = haply::inverse::make_cursor_force(0, 0, force_z);
        client.cursor_set_force(id, cursor_force);

        // Printing too fast can cause problems.
        static size_t step = 0;
        if (((++step) % (haply::inverse::inverse3_max_poll_rate / 100)) > 0)
            return;

        std::fprintf(stdout,
                     "\r"
                     "position=[ % 0.3f % 0.3f % 0.3f ] "
                     "force=[ % 0.3f % 0.3f % 0.3f ]",
                     state.position[0], state.position[1], state.position[2],
                     cursor_force.force[0], cursor_force.force[1],
                     cursor_force.force[2]);
    };

    haply::inverse::client client{std::move(events)};

    try {
        haply::inverse::check(client.connect(), "Unable to connect the client");

        haply::inverse::device_id device_id =
            client.device_open_first(haply_inverse_device_type_inverse3)
                .unwrap("Unable to connect Inverse3");

        fprintf(stdout, "Inverse3 (id:%x)\n", device_id);

        haply::inverse::result<bool> result;
        int nb_err = 0;
        while (nb_err < 10) {
            result = client.poll();
            if (!result) {
                nb_err++;
                std::fprintf(stderr,
                             "\rUnable to poll Inverse3: %s (retry %d...)",
                             haply::inverse::retstr_c(result.error()), nb_err);
                continue;
            }
        }
    } catch (haply::inverse::exception &e) {
        fprintf(stderr, "%s\n", e.what());
        return e.cause;
    }

    return 0;
}
