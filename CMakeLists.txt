# Copyright 2023 Haply Robotics Inc. All rights reserved.

set(CMAKE_WARN_DEPRECATED OFF CACHE BOOL "" FORCE)

cmake_minimum_required(VERSION 3.0.0)
project(HardwareAPI.Examples VERSION 2.0.0 LANGUAGES CXX)

set(HAPLY_PREFIX "c:/Program Files/Haply/Inverse"
    CACHE PATH "Install folder for the SDK")

## Required include and define directives for making use of the API.
include_directories("${HAPLY_PREFIX}/include")
add_definitions(-DUNICODE -D_UNICODE)

## Additional compiler specific directives
if(MSVC)
    add_compile_options(/W3)
else()
    add_compile_options(
        -Wall -Wextra -pedantic
        -Wno-deprecated-declarations)

    if (UNIX)
        add_link_options(-pthread)
    endif()
endif()

link_libraries("${HAPLY_PREFIX}/lib/haply-inverse-runtime.lib")

## Default install path for our example binaries.
if(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
    set(CMAKE_INSTALL_PREFIX "${PROJECT_SOURCE_DIR}" CACHE PATH "..." FORCE)
endif()

file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR})

## Build for the various examples.
add_subdirectory(00-device-list)
add_subdirectory(01-print-inverse3)
add_subdirectory(02-print-verse-grip)
add_subdirectory(03-hello-wall)
add_subdirectory(04-combined)

install(
    FILES "${HAPLY_PREFIX}/lib/haply-inverse-runtime.dll"
    DESTINATION "bin")
